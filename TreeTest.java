import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
/**
 * Sample tests for Tree class.
 * A simplified set of tests intended to demonstrate
 *   how such tests may be written.  More extensive
 *   tests are required to verify correctness.
 * @author  Dr. Jody Paul
 * @version preview (0.2) [30 August 2013]
 */
public class TreeTest {
    /** A tree for testing that stores strings. */
    public Tree<String> stringTree;
    /** String value list for stringTree. */
    public List<String> stringValues;
    /** A tree for testing that stores integers. */
    public Tree<Integer> integerTree;
    /** Integer value list for integerTree. */
    public List<Integer> integerValues;

    /**
     * Default constructor for test class TreeTest.
     * Useful for environment preparation.
     */
    public TreeTest() { }

    /**
     * Set up the test fixture before every test method invocation.
     * This would be a good place for {@link #createTestTrees()}.
     */
    @Before
    public void setUp() {
    }

    /**
     * Tear down the test fixture after every test method completion.
     */
    @After
    public void tearDown() {
    }
    
    /**
     * Utility to set up tree for testing.
     * Because this depends on a working constructor it was not
     *   included as part of {@link #setUp()}.
     */
    public void createTestTrees() {
        stringTree = new Tree<String>("Root");
        List<Tree<String>> schildren = new ArrayList<Tree<String>>();
        Tree<String> schild1 = new Tree<String>("Child 1");
        Tree<String> schild2 = new Tree<String>("Child 2");
        Tree<String> schild3 = new Tree<String>("Child 3");
        schildren.add(schild1);
        schildren.add(schild2);
        schildren.add(schild3);
        stringTree.setRootChildren(schildren);
        Tree<String> schild1a = new Tree<String>("Child 1a");
        Tree<String> schild1b = new Tree<String>("Child 1b");
        Tree<String> schild1c = new Tree<String>("Child 1c");
        List<Tree<String>> schildren1 = new ArrayList<Tree<String>>();
        schildren1.add(schild1a);
        schildren1.add(schild1b);
        schildren1.add(schild1c);
        schild1.setRootChildren(schildren1);
        Tree<String> schild3a = new Tree<String>("Child 3a");
        List<Tree<String>> schildren3 = new ArrayList<Tree<String>>();
        schildren3.add(schild3a);
        schild3.setRootChildren(schildren3);
        //
        stringValues = new ArrayList<String>();
        stringValues.add("Root");
        stringValues.add("Child 1");
        stringValues.add("Child 2");
        stringValues.add("Child 3");
        stringValues.add("Child 1a");
        stringValues.add("Child 1b");
        stringValues.add("Child 1c");
        stringValues.add("Child 3a");
        //
        integerTree = new Tree<Integer>(42);
        List<Tree<Integer>> ichildren = new ArrayList<Tree<Integer>>();
        Tree<Integer> ichild1 = new Tree<Integer>(21);
        Tree<Integer> ichild2 = new Tree<Integer>(63);
        ichildren.add(ichild1);
        ichildren.add(ichild2);
        integerTree.setRootChildren(ichildren);
        Tree<Integer> ichild1a = new Tree<Integer>();
        Tree<Integer> ichild1b = new Tree<Integer>(30);
        List<Tree<Integer>> ichildren1 = new ArrayList<Tree<Integer>>();
        ichildren1.add(ichild1a);
        ichildren1.add(ichild1b);
        ichild1.setRootChildren(ichildren1);
        Tree<Integer> ichild2a = new Tree<Integer>(50);
        Tree<Integer> ichild2b = new Tree<Integer>(70);
        List<Tree<Integer>> ichildren2 = new ArrayList<Tree<Integer>>();
        ichildren2.add(ichild2a);
        ichildren2.add(ichild2b);
        ichild2.setRootChildren(ichildren2);
        //
        integerValues = new ArrayList<Integer>();
        integerValues.add(21);
        integerValues.add(30);
        integerValues.add(42);
        integerValues.add(50);
        integerValues.add(63);
        integerValues.add(70);
    }

    /** Test the no-parameter constructor. */
    @Test
    public void noParameterConstructorTest() {
        Tree<Integer> treeInt = new Tree<Integer>();
        assertEquals(0, treeInt.numberOfNodes());
        Tree<String> treeStr = new Tree<String>();
        assertEquals(0, treeStr.numberOfNodes());
    }

    /** Test the value-parameter constructor. */
    @Test
    public void valueParameterConstructorTest() {
        Tree<Integer> treeInt = new Tree<Integer>(42);
        assertEquals(1, treeInt.numberOfNodes());
        Tree<String> treeStr = new Tree<String>("Forty-Two");
        assertEquals(1, treeStr.numberOfNodes());
    }

    /** Test the copy constructor. */
    @Test
    public void copyConstructorTest() {
        Tree<Integer> treeInt = new Tree<Integer>(42);
        Tree<Integer> copyInt = new Tree<Integer>(treeInt);
        assertEquals(1, copyInt.numberOfNodes());
        assertEquals(42, (int) copyInt.getRootValue());
        Tree<String> treeStr = new Tree<String>("Forty-Two");
        Tree<String> copyStr = new Tree<String>(treeStr);
        assertEquals(1, copyStr.numberOfNodes());
        assertEquals("Forty-Two", copyStr.getRootValue());
        createTestTrees();
        copyInt = new Tree<Integer>(integerTree);
        assertEquals(integerTree.getRootValue(), copyInt.getRootValue());
        copyStr = new Tree<String>(stringTree);
        assertEquals(stringTree.getRootValue(), copyStr.getRootValue());
    }

    /** Test getRootValue. */
    @Test
    public void getRootValueTest() {
        Tree<Integer> itree = new Tree<Integer>(42);
        assertEquals(42, (int) itree.getRootValue());
        Tree<String> stree = new Tree<String>("Forty-Two");
        assertEquals("Forty-Two", stree.getRootValue());
        createTestTrees();
        assertEquals(42, (int) integerTree.getRootValue());
        assertEquals("Root", stringTree.getRootValue());
    }

    /**
     *  Test getRootValue exception.
     */
    @Test (expected = NullPointerException.class)
    public void getRootValueExceptionTest() {
        Tree<Integer> treeInt = new Tree<Integer>();
        treeInt.getRootValue();
    }

    /** Test access and mutation of child subtrees. */
    @Test
    public void rootChildrenTest() {
        Tree<String> stree = new Tree<String>("Root");
        assertEquals(1, stree.numberOfNodes());
        assertEquals(0, stree.getRootChildren().size());
        List<Tree<String>> children = new ArrayList<Tree<String>>();
        children.add(new Tree<String>("Child 1"));
        stree.setRootChildren(children);
        assertEquals(1, stree.getRootChildren().size());
        assertEquals(children, stree.getRootChildren());
        children.add(new Tree<String>("Child 2"));
        children.add(new Tree<String>("Child 3"));
        assertEquals(3, stree.getRootChildren().size());
        stree.setRootChildren(new ArrayList<Tree<String>>());
        assertEquals(0, stree.getRootChildren().size());
        stree.setRootChildren(children);
        assertEquals(3, stree.getRootChildren().size());
    }
    
    /**
     * Test numberOfNodes.
     */
    @Test
    public void numberOfNodesTest() {
        assertEquals(0, (new Tree<String>()).numberOfNodes());
        assertEquals(1, (new Tree<String>("Root")).numberOfNodes());
        createTestTrees();
        assertEquals(8, stringTree.numberOfNodes());
        assertEquals(6, integerTree.numberOfNodes());
    }   

    /**
     * Test numberOfLeafNodes.
     */
    @Test
    public void numberOfLeafNodesTest() {
        assertEquals(0, (new Tree<String>()).numberOfLeafNodes());
        assertEquals(1, (new Tree<String>("Root")).numberOfLeafNodes());
        createTestTrees();
        assertEquals(5, stringTree.numberOfLeafNodes());
        assertEquals(3, integerTree.numberOfLeafNodes());
    }

    /**
     * Test numberOfInternalNodes.
     */
    @Test
    public void numberOfInternalNodesTest() {
        assertEquals(0, (new Tree<String>()).numberOfInternalNodes());
        assertEquals(0, (new Tree<String>("Root")).numberOfInternalNodes());
        createTestTrees();
        assertEquals(3, stringTree.numberOfInternalNodes());
        assertEquals(3, integerTree.numberOfInternalNodes());
    }

    /**
     * Test values() method.
     */
    @Test
    public void valuesTest() {
        createTestTrees();
        assertEquals(8, stringTree.values().size());
        assertTrue(stringTree.values().contains("Root"));
        assertTrue(stringTree.values().containsAll(stringValues));
        assertEquals(6, integerTree.values().size());
        assertTrue(integerTree.values().contains(new Integer(42)));
        assertTrue(integerTree.values().containsAll(integerValues));
    }

    /**
     * Test subtrees() method.
     */
    @Test
    public void subtreesTest() {
        createTestTrees();
        assertEquals(8, stringTree.subtrees().size());
        assertEquals(6, integerTree.subtrees().size());
        assertTrue(stringTree.subtrees().contains(stringTree));
        for (Tree<String> child : stringTree.getRootChildren()) {
            assertTrue(stringTree.subtrees().contains(child));
        }
        assertTrue(integerTree.subtrees().contains(integerTree));
        for (Tree<Integer> child : integerTree.getRootChildren()) {
            assertTrue(integerTree.subtrees().contains(child));
        }
    }

    /**
     * Test breadthFirstValues() method.
     */
    @Test
    public void breadthFirstValuesTest() {
        createTestTrees();
        assertEquals(8, stringTree.breadthFirstValues().size());
        assertTrue(stringTree.breadthFirstValues().contains("Root"));
        assertTrue(stringTree.breadthFirstValues().containsAll(stringValues));
        assertTrue(integerTree.breadthFirstValues().contains(new Integer(42)));
        assertTrue(integerTree.breadthFirstValues().containsAll(integerValues));
        List<String> strVals = stringTree.breadthFirstValues();
        assertEquals("Root", strVals.get(0));
        assertEquals("Child 1", strVals.get(1));
        assertEquals("Child 2", strVals.get(2));
        assertEquals("Child 3", strVals.get(3));
        assertEquals("Child 1a", strVals.get(4));
        assertEquals("Child 1b", strVals.get(5));
        assertEquals("Child 1c", strVals.get(6));
        assertEquals("Child 3a", strVals.get(7));
        List<Integer> intVals = integerTree.breadthFirstValues();
        assertEquals(42, (int) intVals.get(0));
        assertEquals(21, (int) intVals.get(1));
        assertEquals(63, (int) intVals.get(2));
        assertEquals(30, (int) intVals.get(3));
        assertEquals(50, (int) intVals.get(4));
        assertEquals(70, (int) intVals.get(5));
    }

    /**
     * Test depthFirstValues() method.
     */
    @Test
    public void depthFirstValuesTest() {
        createTestTrees();
        assertEquals(8, stringTree.depthFirstValues().size());
        assertTrue(stringTree.depthFirstValues().contains("Root"));
        assertTrue(stringTree.depthFirstValues().containsAll(stringValues));
        assertTrue(integerTree.depthFirstValues().contains(new Integer(42)));
        assertTrue(integerTree.depthFirstValues().containsAll(integerValues));
        List<String> strVals = stringTree.depthFirstValues();
        assertEquals("Root", strVals.get(0));
        assertEquals("Child 1", strVals.get(1));
        assertEquals("Child 1a", strVals.get(2));
        assertEquals("Child 1b", strVals.get(3));
        assertEquals("Child 1c", strVals.get(4));
        assertEquals("Child 2", strVals.get(5));
        assertEquals("Child 3", strVals.get(6));
        assertEquals("Child 3a", strVals.get(7));
        List<Integer> intVals = integerTree.depthFirstValues();
        assertEquals(42, (int) intVals.get(0));
        assertEquals(21, (int) intVals.get(1));
        assertEquals(30, (int) intVals.get(2));
        assertEquals(63, (int) intVals.get(3));
        assertEquals(50, (int) intVals.get(4));
        assertEquals(70, (int) intVals.get(5));
    }
     
    /**
     * Test contains.
     */
    @Test
    public void containsTest() {
        createTestTrees();
        assertTrue(stringTree.contains("Root"));
        for (String s : stringValues) {
            assertTrue(stringTree.contains(s));
        }
        assertTrue(integerTree.contains(new Integer(42)));
        for (Integer i : integerValues) {
            assertTrue(integerTree.contains(i));
        }
    }

    /**
     * Test iterator.
     */
    @Test
    public void iteratorTest() {
        int countSubtrees = 0;
        Tree<String> sstree;
        Tree<Integer> istree;
        createTestTrees();
        Iterator<Tree<String>> sit = stringTree.iterator();
        assertTrue(sit.hasNext());
        for (int i = 0; i < 8; i++) {
            assertTrue(sit.hasNext());
            sstree = sit.next();
            assertTrue(sstree != null);
        }
        assertFalse(sit.hasNext());
        countSubtrees = 0;
        for (Tree<String> ts : stringTree) {
            countSubtrees++;
        }
        assertEquals(8, countSubtrees);
        //
        Iterator<Tree<Integer>> iit = integerTree.iterator();
        assertTrue(iit.hasNext());
        for (int i = 0; i < 6; i++) {
            assertTrue(iit.hasNext());
            istree = iit.next();
            assertTrue(istree != null);
        }
        assertFalse(iit.hasNext());
        countSubtrees = 0;
        for (Tree<Integer> ti : integerTree) {
            countSubtrees++;
        }
        assertEquals(6, countSubtrees);
    }
    
    /**
     * Test toString.
     */
    @Test
    public void toStringTest() {
        createTestTrees();
        System.out.println("Inspect results, no automated assessment.");
        System.out.println("stringTree: " + stringTree);
        System.out.println("integerTree: " + integerTree);
    }

    /**
     * Test equals.
     */
    @Test
    public void equalsTest() {
        Tree<String> emptree1 = new Tree<String>();
        assertTrue(emptree1.equals(emptree1));
        assertEquals(emptree1, emptree1);
        assertFalse(emptree1.equals(null));
        Tree<String> emptree2 = new Tree<String>();
        assertEquals(emptree1, emptree2);
        assertEquals(emptree2, emptree1);
        Tree<Integer> emptree3 = new Tree<Integer>();
        assertEquals(emptree1, emptree3);
        assertEquals(emptree3, emptree1);
        createTestTrees();
        assertEquals(stringTree, stringTree);
        assertEquals(integerTree, integerTree);
        assertFalse(stringTree.equals(emptree1));
        assertFalse(stringTree.equals(integerTree));
        assertFalse(emptree1.equals(stringTree));
        assertFalse(integerTree.equals(stringTree));
        assertEquals(integerTree, new Tree<Integer>(integerTree));
        assertEquals(stringTree, new Tree<String>(stringTree));
        assertFalse(integerTree == new Tree<Integer>(integerTree));
        assertFalse(stringTree == new Tree<String>(stringTree));
    }

    /**
     * Test serialization methods <code>save</code> and <code>restore</code>.
     */
    @Test
    public void serializationTest() throws java.io.IOException {
        Tree emptree1 = new Tree();
        assertTrue(emptree1.save("TreeMT.ser"));
        Tree<Integer> tree42 = new Tree<Integer>(42);
        assertTrue(tree42.save("Tree42.ser"));
        Tree<Integer> treeSR = new Tree<Integer>(21);
        assertTrue(treeSR.save("Tree21.ser"));
        assertEquals(new Integer(21), treeSR.getRootValue());
        assertTrue(treeSR.restore("Tree42.ser"));
        assertEquals(new Integer(42), treeSR.getRootValue());
        assertEquals(tree42, treeSR);
        assertTrue(treeSR.restore("TreeMT.ser"));
        assertEquals(0, treeSR.numberOfNodes());
        assertTrue(tree42.save(null));
        assertTrue(treeSR.restore(null));
        assertEquals(tree42, treeSR);
        createTestTrees();
        assertTrue(stringTree.save("sTree.ser"));
        Tree<String> stSR = new Tree<String>("Discarded");
        assertTrue(stSR.restore("sTree.ser"));
        assertEquals(stringTree, stSR);
        assertTrue(integerTree.save("iTree.ser"));
        assertTrue(treeSR.restore("iTree.ser"));
        assertEquals(integerTree, treeSR);
    }

    /**
     * Test height.
     */
    @Test
    public void heightTest() {
        Tree<Integer> tree1 = new Tree<Integer>();
        assertEquals(0, tree1.height());
        Tree<Integer> tree2 = new Tree<Integer>(42);
        assertEquals(1, tree2.height());
        createTestTrees();
        assertEquals(3, stringTree.height());
        assertEquals(3, integerTree.height());
    }

    /**
     * Test maxDegree.
     */
    @Test
    public void maxDegreeTest() {
        Tree<String> tree1 = new Tree<String>();
        assertEquals(0, tree1.maxDegree());
        Tree<String> tree2 = new Tree<String>("Root");
        assertEquals(0, tree2.maxDegree());
        createTestTrees();
        assertEquals(3, stringTree.maxDegree());
        assertEquals(2, integerTree.maxDegree());
    }
}

